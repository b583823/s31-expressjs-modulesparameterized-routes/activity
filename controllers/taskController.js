// It will allow us to use the contents of the task.js file in the model folder.
const Task = require("../models/task");

module.exports.getAllTasks = () =>{
	// The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventurally to the client/postman.
	return Task.find({}).then(result => result);
}
// Create a task
module.exports.createTask = (reqBody) =>{

	// Create a task object based on the mongoose model "Task"
	let newTask = new Task({
		// Sets the "name" property with the value received from the postman.
		name: reqBody.name
	})
		// Using callback funciton: newUser.save((saveErr, saveTask)=>{})
		// Using .then method: newTask.save().then((Task, error)=>{})
	return newTask.save().then((task,error)=>{
		if(error){
			console.log(error)
			return false
		}
		else{
			return task
		}
	})
}

module.exports.deleteTask = (taskId) =>{
	return Task.findByIdAndRemove(taskId).then((removedTask, err)=>{
		if(err){
			console.log(err);
			return false;
		}
		else{
			return removedTask;
		}
	})
}

module.exports.updateTask = (taskId, reqBody) => {
	return Task.findById(taskId).then((result,error) =>{
		if(error){
			console.log(error);
			return false;
		}
		else{
			// we reassign the result name with the request body content.
			result.name = reqBody.name;
			return result.save().then((updatedTaskName, updateErr)=>{
				if(error){
					console.log(updateErr);
					return false;
				}
				else{
					return updatedTaskName;
				}
			})
		}
	})
}

module.exports.specTask = (taskId) =>{
	return Task.findById(taskId).then(result=>result);
}

module.exports.completeTask =(taskId,reqBody)=>{
	return Task.findById(taskId).then((result,error)=>{
		if(error){
			console.log(error)
			return false;
		}
		else{
			result.status = reqBody.status;
			return result.save().then((updatedStatus, updateStatErr)=>{
				if(error){
					console.log(updateStatErr);
					return false;
				}
				else{
					return updatedStatus;
				}
			})
		}
	})
}